package com.hcl.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeaveMangementApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeaveMangementApplication.class, args);
	}

}
