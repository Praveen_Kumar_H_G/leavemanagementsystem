package com.hcl.learn.exception;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LeaveManagementApplicationException extends RuntimeException {


	private static final long serialVersionUID = 1L;
	private String message;
	private HttpStatus httpStatus;

	public LeaveManagementApplicationException(String message) {
		super(message);
	}
}
