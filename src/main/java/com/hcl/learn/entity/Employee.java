package com.hcl.learn.entity;

import com.hcl.learn.dto.ApiResponse;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Employee {
	@Id
	private long sapId;
	private String name;
	private String email;
	private String password;
	
	private Role role;

}
